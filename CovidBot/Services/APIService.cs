﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CovidBot.Services
{
    public class TicketSummaryResponse
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public Summary data { get; set; }
    }
    public class Summary
    {
        public int OpenTillDate { get; set; }
        public int InProgressTillDate { get; set; }
        public int TodayCreated { get; set; }
        public int TodayClosed { get; set; }
        public int LastWeekCreated { get; set; }
        public int LastWeekClosed { get; set; }
        public int LastMonthCreated { get; set; }
        public int LastMonthClosed { get; set; }
        public int TotalCreated { get; set; }
        public int TotalClosed { get; set; }
    }
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Confirmed
    {
        public int value { get; set; }
        public string detail { get; set; }
    }

    public class Recovered
    {
        public int value { get; set; }
        public string detail { get; set; }
    }

    public class Deaths
    {
        public int value { get; set; }
        public string detail { get; set; }
    }

    public class DailyTimeSeries
    {
        public string pattern { get; set; }
        public string example { get; set; }
    }

    public class CountryDetail
    {
        public string pattern { get; set; }
        public string example { get; set; }
    }

    public class Root
    {
        public Confirmed confirmed { get; set; }
        public Recovered recovered { get; set; }
        public Deaths deaths { get; set; }
        public string dailySummary { get; set; }
        public DailyTimeSeries dailyTimeSeries { get; set; }
        public string image { get; set; }
        public string source { get; set; }
        public string countries { get; set; }
        public CountryDetail countryDetail { get; set; }
        public DateTime lastUpdate { get; set; }
    }


    public class APIService
    {
        public static async Task<Root> GetReport(string baseUrl, string localPath)
        {
            using (var client = new HttpClient())
            {
                try
                {
                    client.BaseAddress = new Uri(baseUrl);
                    client.DefaultRequestHeaders.Accept.Clear();

                    var httpResponse = client.GetAsync(localPath).Result;
                    httpResponse.EnsureSuccessStatusCode();
                    var actualData = await httpResponse.Content.ReadAsStringAsync();
                    Root response = JsonConvert.DeserializeObject<Root>(actualData);
                    return response;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
