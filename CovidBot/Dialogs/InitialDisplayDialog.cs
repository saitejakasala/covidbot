﻿using CovidBot.Cards;
using CovidBot.Services;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CovidBot.Dialogs
{
    public class InitialDisplayDialog:ComponentDialog
    {
        public InitialDisplayDialog(string dialogId,IConfiguration configuration) : base(dialogId)
        {
            InitializeWaterfallDialog();
        }

        private void InitializeWaterfallDialog()
        {
            // Create Waterfall Steps
            var waterfallSteps = new WaterfallStep[]
            {
                InitialStepAsync,
            };

            // Add Named Dialogs
            AddDialog(new WaterfallDialog($"{nameof(InitialDisplayDialog)}.mainFlow", waterfallSteps));
            AddDialog(new TextPrompt($"{nameof(InitialDisplayDialog)}.data"));


            // Set the starting Dialog
            InitialDialogId = $"{nameof(InitialDisplayDialog)}.mainFlow";
        }

        private async Task<DialogTurnResult> InitialStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            var summary = APIService.GetReport("https://covid19.mathdro.id",$"/api").Result;
            var reply = MessageFactory.Text("");
          
            reply.Attachments.Add(Reports.GetReportsCard(summary,"GLOBAL"));

            await stepContext.Context.SendActivityAsync(reply, cancellationToken);

            var card = CardActivityAsync(stepContext, false, cancellationToken);
            await stepContext.PromptAsync($"{nameof(InitialDisplayDialog)}.data", new PromptOptions()
            {
                Prompt = (Activity)MessageFactory.Attachment(card.ToAttachment())
            }, cancellationToken);

            return await stepContext.CancelAllDialogsAsync(cancellationToken);

        }
        private HeroCard CardActivityAsync(WaterfallStepContext turnContext, bool update, CancellationToken cancellationToken)
        {
            var heroCard = new HeroCard
            {

                Buttons = new List<CardAction>
                        {
                            new CardAction
                            {
                                Type = ActionTypes.MessageBack,
                                Title = "By Country wise",
                                Text = "Please show data by country wise",
                                DisplayText="Please show data by country wise"
                            }
                        }
            };
            return heroCard;
        }



    }
}
