﻿using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace CovidBot.Dialogs
{
    public class MainDialog : ComponentDialog
    {
        #region Variables
        private readonly IConfiguration _config;
        private readonly string[] messages = { "hi", "hello", "hie", "covid", "hey there" };
        #endregion  

        public MainDialog(IConfiguration config) : base(nameof(MainDialog))
        {
            _config = config;
            InitializeWaterfallDialog();
        }

        private void InitializeWaterfallDialog()
        {
            // Create Waterfall Steps
            var waterfallSteps = new WaterfallStep[]
            {
                InitialStepAsync,
                FinalStepAsync
            };
            // Add Named Dialogs
            AddDialog(new InitialDisplayDialog($"{nameof(MainDialog)}.initialdisplay", _config));
            AddDialog(new CountryDataDialog($"{nameof(MainDialog)}.countrydata",_config));
           
            AddDialog(new WaterfallDialog($"{nameof(MainDialog)}.mainFlow", waterfallSteps));

            //Display user name prompt 
            AddDialog(new TextPrompt($"{nameof(MainDialog)}.prompt"));

            // Set the starting Dialog
            InitialDialogId = $"{nameof(MainDialog)}.mainFlow";
        }

        private async Task<DialogTurnResult> InitialStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
           
            var message = Regex.Replace(stepContext.Context.Activity.Text.ToLower().Trim(), @"\t\n\r", "");

            if (messages.Contains(message))
            {
                await stepContext.PromptAsync($"{nameof(MainDialog)}.prompt", new PromptOptions()
                {
                    Prompt = MessageFactory.Text("Welcome to the COVID-19 Bot")
                }, cancellationToken);
                await stepContext.EndDialogAsync(null, cancellationToken);
                return await stepContext.BeginDialogAsync($"{nameof(MainDialog)}.initialdisplay", null, cancellationToken);

            }
            else if (message == "Please show data by country wise".ToLower())
            {
                return await stepContext.BeginDialogAsync($"{nameof(MainDialog)}.countrydata", null, cancellationToken);
            }
            else
            {
                await stepContext.PromptAsync($"{nameof(MainDialog)}.prompt", new PromptOptions()
                {
                    Prompt = MessageFactory.Text("Invalid request. Please type COVID to access the home menu.")
                }, cancellationToken);

                return await stepContext.EndDialogAsync(null, cancellationToken);
            }
        }

        private async Task<DialogTurnResult> FinalStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.EndDialogAsync(null, cancellationToken);
        }
       
    }
}
