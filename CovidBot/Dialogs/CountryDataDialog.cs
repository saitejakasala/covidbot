﻿using CovidBot.Cards;
using CovidBot.Services;
using Microsoft.Bot.Builder;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Schema;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CovidBot.Dialogs
{
    public class CountryDataDialog : ComponentDialog
    {
        public CountryDataDialog(string dialogId, IConfiguration configuration) : base(dialogId)
        {

            InitializeWaterfallDialog();
        }

        private void InitializeWaterfallDialog()
        {
            // Create Waterfall Steps
            var waterfallSteps = new WaterfallStep[]
            {
                CountryNameStepAsync,
                FinalStepAsync
            };

            // Add Named Dialogs
            AddDialog(new WaterfallDialog($"{nameof(CountryDataDialog)}.mainFlow", waterfallSteps));
            AddDialog(new TextPrompt($"{nameof(CountryDataDialog)}.data"));
            AddDialog(new TextPrompt($"{nameof(CountryDataDialog)}.countryName"));


            // Set the starting Dialog
            InitialDialogId = $"{nameof(CountryDataDialog)}.mainFlow";
        }

        private async Task<DialogTurnResult> InitialStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.CancelAllDialogsAsync(cancellationToken);
        }
        private async Task<DialogTurnResult> CountryNameStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            return await stepContext.PromptAsync($"{nameof(CountryDataDialog)}.countryName",
                new PromptOptions
                {
                    Prompt = MessageFactory.Text("Enter Country name.")
                }, cancellationToken);
        }
        private async Task<DialogTurnResult> FinalStepAsync(WaterfallStepContext stepContext, CancellationToken cancellationToken)
        {
            stepContext.Values["countryName"] = (string)stepContext.Result;
            string localPath = stepContext.Values["countryName"].ToString();

            var summary = APIService.GetReport("https://covid19.mathdro.id",$"/api/countries/{localPath.ToUpper()}").Result;
            var reply = MessageFactory.Text("");

            reply.Attachments.Add(Reports.GetReportsCard(summary,localPath.ToUpper()));

            await stepContext.Context.SendActivityAsync(reply, cancellationToken);

            var card = CardActivityAsync(stepContext, false, cancellationToken);
            await stepContext.PromptAsync($"{nameof(CountryDataDialog)}.data", new PromptOptions()
            {
                Prompt = (Activity)MessageFactory.Attachment(card.ToAttachment())
            }, cancellationToken);

            return await stepContext.CancelAllDialogsAsync(cancellationToken);
        }
        private HeroCard CardActivityAsync(WaterfallStepContext turnContext, bool update, CancellationToken cancellationToken)
        {
            var heroCard = new HeroCard
            {

                Buttons = new List<CardAction>
                        {
                            new CardAction
                            {
                                Type = ActionTypes.MessageBack,
                                Title = "By Country wise",
                                Text = "Please show data by country wise",
                                DisplayText="Please show data by country wise"
                            }
                        }
            };
            return heroCard;
        }
    }
}
