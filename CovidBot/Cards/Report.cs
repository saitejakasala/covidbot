﻿using AdaptiveCards;
using CovidBot.Services;
using Microsoft.Bot.Schema;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CovidBot.Cards
{
    public class Reports
    {
        public static Attachment GetReportsCard(Root reportData,string country)
        {
            var description = "Number of active cases of COVID-19";
            var summary = reportData;
            var card = new AdaptiveCard("1.0");
            List<AdaptiveElement> AdaptiveElementRemaining1 = new List<AdaptiveElement>
        {
        new AdaptiveColumnSet()
        {
            Columns = new List<AdaptiveColumn>()
            {
                new AdaptiveColumn()
                {
                    Items = new List<AdaptiveElement>()
                    {
                        new AdaptiveTextBlock()
                        {
                            Text= $"**{country} COVID-19 Report**",
                            Weight= AdaptiveTextWeight.Bolder,
                            Size = AdaptiveTextSize.ExtraLarge,
                        },
                    },
                }
            }
        },
        new AdaptiveColumnSet()
        {

            Separator=true,
            Columns = new List<AdaptiveColumn>()
            {
                new AdaptiveColumn()
                {
                    Items = new List<AdaptiveElement>()
                    {
                        new AdaptiveTextBlock()
                        {
                            Text= $"Confirmed",
                            Weight= AdaptiveTextWeight.Bolder,
                            Size = AdaptiveTextSize.Medium,
                            HorizontalAlignment=AdaptiveHorizontalAlignment.Center
                        },
                    }
                },
                new AdaptiveColumn()
                {
                    Items = new List<AdaptiveElement>()
                    {
                        new AdaptiveTextBlock()
                        {
                            Text= $"**Recovered**",
                            Weight= AdaptiveTextWeight.Bolder,
                            Size = AdaptiveTextSize.Medium,
                            HorizontalAlignment=AdaptiveHorizontalAlignment.Center
                        },
                    }
                },new AdaptiveColumn()
                {
                    Items = new List<AdaptiveElement>()
                    {
                        new AdaptiveTextBlock()
                        {
                            Text= $"**Deaths**",
                            Weight= AdaptiveTextWeight.Bolder,
                            Size = AdaptiveTextSize.Medium,
                            HorizontalAlignment=AdaptiveHorizontalAlignment.Center
                        },
                    }
                },

            },
        },
        new AdaptiveColumnSet()
        {
            Columns = new List<AdaptiveColumn>()
            {
                new AdaptiveColumn()
                {
                    Items = new List<AdaptiveElement>()
                    {
                        new AdaptiveTextBlock()
                        {
                            Text= $"{summary.confirmed.value}",
                            Size = AdaptiveTextSize.Medium,
                            HorizontalAlignment=AdaptiveHorizontalAlignment.Center
                        },
                    }
                },
                new AdaptiveColumn()
                {
                    Items = new List<AdaptiveElement>()
                    {
                        new AdaptiveTextBlock()
                        {
                            Text= $"{summary.recovered.value}",
                            Size = AdaptiveTextSize.Medium,
                            HorizontalAlignment=AdaptiveHorizontalAlignment.Center
                        },
                    }
                },
                new AdaptiveColumn()
                {
                    Items = new List<AdaptiveElement>()
                    {
                        new AdaptiveTextBlock()
                        {
                            Text= $"{summary.deaths.value}",
                            Size = AdaptiveTextSize.Medium,
                            HorizontalAlignment=AdaptiveHorizontalAlignment.Center
                        },
                    }
                }
            }
        },
        new AdaptiveColumnSet()
        {
            Columns = new List<AdaptiveColumn>()
            {
                new AdaptiveColumn()
                {
                    Items = new List<AdaptiveElement>()
                    {
                        new AdaptiveTextBlock()
                        {
                            Text= $"{string.Format("{0:g}", summary.lastUpdate)}",
                            Size = AdaptiveTextSize.Medium,
                            HorizontalAlignment=AdaptiveHorizontalAlignment.Center
                        },
                    }
                },
                new AdaptiveColumn()
                {
                    Items = new List<AdaptiveElement>()
                    {
                        new AdaptiveTextBlock()
                        {
                            Text= $"{string.Format("{0:g}", summary.lastUpdate)}",
                            Size = AdaptiveTextSize.Medium,
                            HorizontalAlignment=AdaptiveHorizontalAlignment.Center
                        },
                    }
                },
                new AdaptiveColumn()
                {
                    Items = new List<AdaptiveElement>()
                    {
                        new AdaptiveTextBlock()
                        {
                            Text= $"{string.Format("{0:g}", summary.lastUpdate)}",
                            Size = AdaptiveTextSize.Medium,
                            HorizontalAlignment=AdaptiveHorizontalAlignment.Center
                        },
                    }
                }
            }
        },
        new AdaptiveColumnSet()
        {
            Columns = new List<AdaptiveColumn>()
            {
                new AdaptiveColumn()
                {
                    Items = new List<AdaptiveElement>()
                    {
                        new AdaptiveTextBlock()
                        {
                            Text= $"{description}",
                            Size = AdaptiveTextSize.Medium,
                            Wrap=true,
                            HorizontalAlignment=AdaptiveHorizontalAlignment.Center
                        },
                    }
                },
                new AdaptiveColumn()
                {
                    Items = new List<AdaptiveElement>()
                    {
                        new AdaptiveTextBlock()
                        {
                            Text= $"{description}",
                            Wrap=true,
                            Size = AdaptiveTextSize.Medium,
                            HorizontalAlignment=AdaptiveHorizontalAlignment.Center
                        },
                    }
                },
                new AdaptiveColumn()
                {
                    Items = new List<AdaptiveElement>()
                    {
                        new AdaptiveTextBlock()
                        {
                            Text= $"{description}",
                            Wrap=true,
                            Size = AdaptiveTextSize.Medium,
                            HorizontalAlignment=AdaptiveHorizontalAlignment.Center
                        },
                    }
                }
            }
        }
        

    };

            AdaptiveContainer adaptiveContainerRemaining1 = new AdaptiveContainer();
            adaptiveContainerRemaining1.Items = AdaptiveElementRemaining1;
            //adaptiveContainerRemaining1.Items.AddRange(AdaptiveElementComments);

            card.Body.Add(adaptiveContainerRemaining1);
            Attachment attachment = new Attachment()
            {
                ContentType = AdaptiveCard.ContentType,
                Content = JsonConvert.DeserializeObject(JsonConvert.SerializeObject(card))
            };
            return attachment;
        }
    }
}
